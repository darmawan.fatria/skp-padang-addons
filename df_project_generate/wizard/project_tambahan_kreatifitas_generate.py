from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime,timedelta
import time
from mx import DateTime

class project_tambahan_kreatifitas_generate(osv.Model):
    _name = "project.tambahan.kreatifitas.generate"
    _description = "Generate Tugas Tambahan"
    
    _columns = {
        'name'     : fields.char('Nama Kegiatan', size=128, required=True),
        'lama_kegiatan'     : fields.integer('Lama Kegiatan',readonly=True),
        'user_id': fields.many2one('res.users', 'Penanggung Jawab', ),
        'user_id_atasan': fields.many2one('res.users', 'Pejabat Penilai', ),
        'user_id_banding': fields.many2one('res.users', 'Atasan Pejabat Penilai', ),
        'user_id_bkd': fields.many2one('res.users', 'Verifikatur', ),
        'satuan_lama_kegiatan'     : fields.selection([('bulan', 'Bulan')],'Satuan Waktu Lama Kegiatan',readonly=True),
        'date_start'     : fields.date('Periode Awal Generate'),
        'target_period_year'     : fields.char('Periode Tahun', size=4, required=True),
        
        }
    _defaults = {
        'name' : "Realisasi Bulan ",
        'lama_kegiatan' : 12,
        'satuan_lama_kegiatan':'bulan',
        'target_period_year':lambda *args: time.strftime('%Y'),
        'user_id': lambda self, cr, uid, ctx: uid,
       
        }
    def generate_task_realisasi_bulanan(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        task =  {}
        task_pool = self.pool.get('project.tambahan.kreatifitas')
        user_pool = self.pool.get('res.users')
        
        for task_generate in self.browse(cr, uid, ids, context=context):
            #check Duplicate
            #Init Field
            user_id = task_generate.user_id.id
            user_obj = task_generate.user_id
            target_category='bulanan'
            description=''
            lama_kegiatan=task_generate.lama_kegiatan
            
            target_period_year = task_generate.target_period_year
            target_period_month='xx'
            date_start='xx'
            date_end='xx'
            company_id=None
            currency_id=None
            user_id_bkd=None
            employee = user_obj.partner_id
            if user_id!=uid:
              raise osv.except_osv(_('Invalid Action!'),
                                             _('Anda Tidak Memiliki Priviledge Untuk Proses Ini.'))
            if not employee :
                raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Ada Beberapa Informasi Kepegawaian Belum Diisi, Khususnya Data Pejabat Penilai Dan Atasan Banding.'))
            else :
                company = employee.company_id
                company_id = company.id
                currency_id= employee.company_id.currency_id.id
                
                #print "company_id : ",company_id,' - ',currency_id
                
                if not company_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Unit Dinas Pegawai Belum Dilengkapi.'))
                #print "employee parent : ",employee.parent_id
                if not task_generate.user_id_bkd:
                    if not company.user_id_bkd :
                        raise osv.except_osv(_('Invalid Action, Data Dinas Kurang Lengkap'),
                                    _('Staff Pemeriksa  Tidak Tersedia Untuk Unit Anda, Silahkan hubungi Admin Atau isi Data Pemeriksa.'))
                    else :
                        user_id_bkd = company.user_id_bkd.id
                else :
                    user_id_bkd=task_generate.user_id_bkd.id 
                if not employee.user_id_atasan :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Pejabat Penilai Belum Terisi.'))
                if not employee.user_id_banding :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Pejabat Pengajuan Banding.'))
                if not employee.job_type or not employee.job_id or not employee.golongan_id :
                    raise osv.except_osv(_('Invalid Action, Data Pegawai Tidak Lengkap'),
                                _('Proses Tidak Dapat Dilanjutkan Karena Data Jabatan Dan Pangkat Belum Diiisi.'))
            user_id_atasan =task_generate.user_id_atasan.id
            user_id_banding=task_generate.user_id_banding.id 
            
            if not task_generate.user_id_atasan.id :
                user_id_atasan = employee.user_id_atasan.user_id.id
            if not task_generate.user_id_banding.id :
                user_id_banding = employee.user_id_banding.user_id.id
            
            task.update({
                           'user_id':user_id,
                           'company_id':company_id,
                           'description':description,
                           'name': task_generate.name,
                           'code': None,
                           'target_period_year': target_period_year,
                            'user_id_atasan': user_id_atasan or False,
                            'user_id_banding':user_id_banding or False,
                            'user_id_bkd':user_id_bkd or False,
                            'currency_id':currency_id,
                            'active':True,
                           })
            #Update Task Target Bulanan
            now=DateTime.today();
            first_task_id=None
            
            if task_generate.date_start :
                curr_date = DateTime.strptime(task_generate.date_start,'%Y-%m-%d')
            else :
                january=DateTime.Date(now.year,1,1)
                curr_date =  DateTime.strptime(january.strftime('%Y-%m-%d'),'%Y-%m-%d')
            first_date =curr_date
            #print "THIS IS A DATE ",curr_date
            for i in range(0,lama_kegiatan):
                
                next_date = curr_date + DateTime.RelativeDateTime(months=i)
                target_period_month=next_date.strftime('%m')
                task.update({
                           'target_period_month':target_period_month,
                           'name': '%s %s' % (task_generate.name,target_period_month),
                 })
                #Check Duplicate Do Not Create
                task_ids = task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_month','=',target_period_month),('target_period_year','=',target_period_year),
                                                ('state','!=','draft')], context=None)
                if task_ids:
                    continue;
                else : 
                    #Delete Duplicate
                    task_ids = task_pool.search(cr, uid, [('user_id','=',user_id),('target_period_month','=',target_period_month),('target_period_year','=',target_period_year),
                                                          ('state','=','draft')], context=None)
                    task_pool.action_cancel_realisasi(cr, uid, task_ids, context=None)
            
                date_start='xx'
                date_end='xx'
                state='draft';
                task.update({
                                'state':state,
                        })
                #insert task
                task_id = task_pool.create(cr, uid, task,context)
                
            
            
        return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.message.task',
                'target': 'new',
                'context': {
                    'default_message_info': 'Tugas Tambahan Dan Kreatifitas Bulanan Berhasil Digenerate',
                    }
            }

project_tambahan_kreatifitas_generate()
class notification_message_task(osv.osv_memory):
    _name = 'notification.message.task'
    _description = 'Pesan Popup Notifikasi'
    _columns = {
        'message_info': fields.text('Pesan'),
        }
notification_message_task()    